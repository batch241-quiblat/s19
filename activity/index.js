let username;
let password;
let role;

function userLogin(){
	username = prompt("Enter username:");
	password = prompt("Enter password:");
	role = prompt("Enter role:").toLowerCase();

	if (username == "" || username == null){
		alert("Username should not be empty");
	}
	else if(password == "" || password == null){
		alert("Password should not be empty");
	}
	else if(role == "" || role == null){
		alert("Role should not be empty");
	}else{
		switch(role){
			case "admin":
				alert("Welcome back to the class portal, " + role + "!");
				break;
			case "teacher":
				alert("Thank you for logging in, " + role + "!");
				break;
			case "student":
				alert("Welcome to the class portal, " + role + "!");
				break;
			default:
				alert("Role out of range.");
				break;
		}
	}
}

function checkAverage(number1, number2, number3, number4){
	average = Math.round((number1 + number2 + number3 + number4) / 4);

	if(average <= 74){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is F");
	}
	else if(average >= 75 && average <= 79){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is D");
	}
	else if(average >= 80 && average <= 84){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is C");
	}
	else if(average >= 85 && average <= 89){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is B");
	}
	else if(average >= 90 && average <= 95){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A");
	}
	else if(average >= 96){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");
	}
}

userLogin();